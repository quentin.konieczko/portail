Bloc 2 - Algorithmique
======================

[DIU Enseigner l'informatique au lycée, Univ. Lille](../Readme.md)

Intervenants - contacts
=======================

Benoit Papegay,
Bruno Bogaert,
Éric Wegrzynowski,
Laetitia Jourdan,
Lucien Mousin,
Marie-Émilie Voge,
Patricia Everaere,
Philippe Marquet,
Sophie Tison

Algorithmes de tri
==================

Séance 1. 

* [Activité d'informatique sans ordinateur - les tris](tri-sans-ordi/Readme.md)
* [Reconnaître les tris](tri_anonymes/Readme.md)

Séance 2

*[Analyser les tris](analyse_tris/Readme.md)

