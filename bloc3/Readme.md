Bloc 3 - Architectures matérielles et robotique, systèmes et réseaux
====================================================================

[DIU Enseigner l'informatique au lycée, Univ. Lille](../Readme.md)

Intervenants - contacts
=======================

Benoit Papegay,
Jean-François Roos,
Jean-Luc Levaire,
Laurent Noé,
Philippe Marquet,
Yvan Peter

Et
Gilles Grimaud

Mais aussi
Pierre Boulet,
Thomas Vantroys,
Yann Secq

Prise en main de l'environnement
================================

* [Initiation à UNIX, à l'interpréteur de commandes](seance0/shell.md)
  - Très et trop brève introduction
  - Découverte de l'interpréteur de commandes
* [GitLab](seance0/gitlab.md)
  - Découverte de l'environnement GitLab
* [Initiation à UNIX, à l'interpréteur de commandes](seance0/shell.md)
  - Utiliser l'interpréteur de commandes
  - ...

Les systèmes d'exploitation
===========================

amphi

Machine virtuelle
=================

* [Installation de VirtualBox](virtualbox/seance-virtualisation.md)

Interpréteur de commandes
=========================

* [Initiation à UNIX, à l'interpréteur de commandes](seance0/shell.md)
  - (suite et fin)


Ressources
==========

Carte de référence Unix de Moïse Valvassori

* [unix-refcard.pdf sur www.ai.univ-paris8.fr](http://www.ai.univ-paris8.fr/~djedi/poo/unix-refcard.pdf)
* [copie locale](doc/unix-refcard.pdf)

