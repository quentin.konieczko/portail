Bloc 1 - Représentation des données et programmation
====================================================

[DIU Enseigner l'informatique au lycée, Univ. Lille](../Readme.md)

Intervenants - contacts
=======================

Patricia Everaere,
Mikaël Salson,
Benoit Papegay,
Eric Wegrzynowski,
Philippe Marquet,
Jean-Christophe Routier,
Bruno Bogaert,
Maude Pupin,
Jean-François Roos,
Fabien Delecroix

9 mai - Premiers pas avec Thonny et Python
==================================

* [Prise en main de Thonny](thonny/readme.md)
* [Traitement de séquences ADN](adn/readme.md)
* [Jeu de la vie](jeu-de-la-vie/readme.md)

15 mai
======

* Bonnes pratiques de programmation Python
  - amphi 
  - doctest, mode pas à pas
  - modules 
* Jeu de la vie à terminer
* Générateur de phrases de passe

Travail pour le 5 juin
======================

* [Wator - simulation proie-prédateur](wator/readme.md)
  - progression avec les élèves

5 juin
======

* Module et dictionnaires 
  - amphi
* Évaluation / discussion Wator

Ressources
==========

* [Memento Python3](https://perso.limsi.fr/pointal/_media/python:cours:mementopython3.pdf)
* [Python3 en résumé](doc/python3_resume.md)
